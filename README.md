Nama anggota kelompok :
1. Bintang Ilham Syahputra
2. Muhammad Raffi Akbar
3. Shavira Adeva
4. Yumna Pratista Tastaftian

Status Pipelines :
[![pipeline status](https://gitlab.com/adeva/aku-dan-kamu/badges/master/pipeline.svg)](https://gitlab.com/adeva/aku-dan-kamu/commits/master)

Coverage Report :
[![coverage report](https://gitlab.com/adeva/aku-dan-kamu/badges/master/coverage.svg)](https://gitlab.com/adeva/aku-dan-kamu/commits/master)

Link HerokuApp :
https://kamibahagia.herokuapp.com/