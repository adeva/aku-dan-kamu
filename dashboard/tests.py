from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views  import index
from Profile.models import Account
from add_friends.models import Teman
from update_status.models import Message
#from add_friends import Teman
# Create your tests here.
class dashboardUnitTest(TestCase):
	def setUp(self):
		self.Account = Account.objects.create(name = 'Yumna Pratista Tastaftian', birth = '3 August', gender = 'Male', Expertise = 'Memancing, Berkuda, Basket',
                                             Description = 'Aku botak. Aku cinta padamu sampai kaki.', Email = 'yumnanaruto@gmail.com')
		self.Teman = Teman.objects.create(nama_teman="Bintang Ilham Syahputra",url_teman="bismillahbisa.herokuapp.com")

	def test_dashboard_url_is_exist(self):
	    response = Client().get('/dashboard/')
	    self.assertEqual(response.status_code,200)

	#Test function index untuk dashboard
	def test_dashboard_using_index(self):
		found = resolve('/dashboard/')
		self.assertEqual(found.func,index)

	def test_if_header_is_exist(self):
		response = Client().get('/dashboard/')
		html_response = response.content.decode('utf8')
		self.assertIn("navbar-yumna",html_response)

	def test_if_footer_is_exist(self):
		response = Client().get('/dashboard/')
		html_response = response.content.decode('utf8')
		self.assertIn("kamibahagia",html_response)

	def test_if_jumlahTeman_is_in_request(self):
		jumlah_awal = Teman.objects.count()
		teman_baru = Teman.objects.create(nama_teman="Bintang Ilham Syahputra",url_teman="bismillahbisa.herokuapp.com")
		jumlah_akhir = Teman.objects.count()
		self.assertEqual(jumlah_awal+1,jumlah_akhir)

	def test_if_status_is_in_request(self):
		status = Message.objects.create(message="Bismillah berhasil")
		hasil = Message.objects.first()

		response = Client().get('/dashboard/')
		self.assertEqual(status,hasil)


	# def test_if_jumlahFeeds_is_in_request(self):


#funtion untuk mengecek apakah jumlah teman yang sesuai dengan jumlah teman dari si orang
	# def test_if_friend_is_equal(self):
	# 	teman_baru = Teman.objects.
	# 	return
