from django.shortcuts import render
from Profile.models import Account
from add_friends.models import Teman
from update_status.models import Message
# Create your views here.
response = {}
def index(request):
	total_teman = Teman.objects.count()
	status = Message.objects.all()
	jumlah_post = status.count()

	if (jumlah_post==0):
		last_post = ""
		response['status'] = last_post
	else:
		last_post = status[status.count()-1]
		response['status'] = last_post.message
		response['tanggal_status'] = last_post.created_date

	response['nama'] = Account.objects.first().name
	response['total_teman'] = total_teman
	response['jumlah_post'] = jumlah_post
	return render(request,'dashboard.html',response)
